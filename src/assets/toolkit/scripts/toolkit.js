'use strict';
/**
 * Toolkit JavaScript
 */

console.log('codepen js');
var $j = jQuery.noConflict();
//create popup
var popup = '<div class="tiny reveal" id="exampleModal" data-reveal><p>OH IM SO FUUUUL</p><img src="http://placekitten.com/1920/1280" alt="Intropsective Cage"><button class="close-button" data-close aria-label="Close reveal" type="button"><span aria-hidden="true">&times;</span></button></div>',
    tocClass = 'prx-toc';

//$('body').append(popup);



//steps introduction
if ($j('.tut-limit.start').length && $j('.tut-limit.stop')) {
    $j(".tut-limit.start").nextUntil(".tut-limit.stop").wrapAll('<div class="prx-wrapper prx-learning-block" />');

    if ($j('.prx-learning-block .toc').length) {

        $j('.prx-learning-block  h2').each(function () {
            $j(this).attr('data-magellan-target', ($j(this).attr('id')));
            $j(this).nextUntil("h2, main").andSelf().wrapAll('<section class="prx-learning-block__section" />');

        });


        //create grid
        $j('.prx-learning-block .toc').wrap('<div class="prx-small-12 prx-medium-3 prx-columns"></div>');
        $j('.prx-learning-block .prx-learning-block__section').wrapAll('<div class="prx-small-12 prx-medium-9 prx-columns" />');
        $j('.prx-learning-block .prx-columns').wrapAll('<div class="prx-row prx-wrapper prx-small-collapse prx-medium-uncollapse"></div>');

        $j('.toc').addClass(tocClass);
        var tocHeight = $j('.toc').innerHeight() + 30,
            viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
            tocTitle = $j(".j-content-header h1").text();
        if (viewportWidth > 640) {
            $j('.' + tocClass).attr('data-magellan', '')
                .prepend('<div class="prx-toc-title-repeat">' + tocTitle + '</strong>')
                .scrollToFixed({
                    marginTop: 10,
                    limit: $j($j('.jive-attachments')[0]).offset().top - tocHeight
                });
        }
    }


}

//initilize foundation
$j(document).foundation();
//$j('.toc').foundation('reflow');
//$j('.j-globalNavLink').on('click',function(){
///  ('#j-nav-search').trigger('click');

//})
//open popup

// $j('#exampleModal').foundation('open');