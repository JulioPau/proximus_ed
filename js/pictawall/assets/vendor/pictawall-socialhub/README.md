# Pictawall - Social Hub Widget
## Usage
The `dist` folder contains a production-ready version of the Social Hub, an example on how to install and configure it is available in the `example` folder.

To install the widget, you just need to add the `pictahub.min.js` script to your web page. That script will expose a function called `pictawall.SocialHub` to the global object, that you will have to invoque to configure the hub.  
*Note: It is fundamental that you do not modify the internal structure of the dist folder and do not rename the main script as it is used to determine where the additionnal dependencies we need are located.*

## Compatibility

* Safari 6.2  
* Chrome 26  
* Firefox 25  
* Opera 25  
* IE 11 (bug dans babel pour IE < 11) // https://phabricator.babeljs.io/T3041  

## Configuration
Setting up your script is very straightforward, pass the function an object containing the parameters.

```
#!javascript
// Configuration - example
pictawall.SocialHub({
    container: '#pictawall',
    eventIdentifier: 'social-media-night-stuttgart-1',
});
```

### Parameters
* `container` [HTMLElement|String] (required): The element (or a css selector pointing to the element) to which the widget will be added.  
* `eventIdentifier` [string] (required): The identifier of the event you wish to display.  
* `options` [object]: An object containing optionnal configuration options. Those are listed bellow:
    * `adRatio` [integer] (default=25): How many cards should we display before displaying an ad. Set to 0 to disable ads.
    * `cssNamespace` [string] (default=pictawall-socialhub): All css classes used by the hub are prefixed by this string.
    * `_layout` [boolean|object] (default=true): False: disables the default design, true: enables the default design with default options, object: options for the default design
        * `mediumBatchSize` [integer] (default=25): Used by the default design - How many cards should be loaded at once when adding cards to the page.
        * `ctas` [object[]] (default=[]): Used by the default design - Calls to action to add to the cards. Each object must contain a `text` (CTA display text) and `url` (where to go when the CTA has been clicked) attribute
        * `plugins` [<{ plugin: string|function, config: object }|string|function>[]] (default=['autolinker', 'modal', 'infinite-scroll']): A list of plugins that will hook into the default design to add some functionality. See [Plugins](#markdown-header-plugins) for more information.

### Plugins
Plugins are used to add functionality to the default design. You can declare a plugin by 3 different ways.

* As a string, which is the name of one of the [default plugins](#markdown-header-default-plugins).
* As a function or an ES6 class. For details on how to create a new plugin, look at the default plugins' source code in `src/js/display/default_design/*Plugin.js`
* As an object containing the properties `plugin` (which is one of the two above) and `config` which is an object that will be given to the plugin when instantiating it.

```
#!javascript
// Plugins - example
pictawall.SocialHub({
    /* only the relevant part has been added */
    _layout: {
        plugins: [
            'autolinker',
            'modal',
            {
                plugin: 'infinite-scroll', 
                config: {
                    scrollEndMessage: 'You\'ve reached the end of the content!'
                }
            }
        ]
    }
});
```

### Default plugins

* `autolinker`: Runs [Autolinker](https://github.com/gregjacobs/Autolinker.js/) on the contents of the cards. Loaded by default.
* `modal`: Display the contents of card in a modal when a user clicks one. Loaded by default. Possible config options are:
    * `cssOverride` [function]: A function that will handle loading the css.
* `load-more-button`: Adds a button after the list of cards that the user can click to load more cards.
* `infinite-scroll`: Loads new cards when the user scrolls past the end of those displayed. Loaded by default. Possible configuration options are:
    * `scrollEndMessage` [string] (default=''): Message displayed when there is no more card available for display.
    * `scrollCheckerElement` [HTMLElement|string] (default=document.body): A css selector or an html element. The element on which the scroll event listener should be added.
    * `scrollChecker` [function]: A method that returns `true` if more cards should be loaded and `false` otherwise. The default method checkes if ${scrollCheckerElement} is nearly at the end of its scroll.
* `defaultdesign-css` Loads the CSS for the default design, allows replacing the default css by a custom one. Always loaded. Options are:
    * `cssOverride` [function(callback)]: A function that will handle loading the css.

Note: functions may return promises, the system will wait for them to resolve.

### Script Hash Configuration
The Social Hub also supports putting the script directly in the script url, after the hash. It must be in a json format and encoded in base64.

```
#!html
<!-- Translates to {"container":"#pictawall","eventIdentifier":"magritte"} -->
<script src="assets/pictahub/pictahub.min.js#eyJjb250YWluZXIiOiIjcGljdGF3YWxsIiwiZXZlbnRJZGVudGlmaWVyIjoibWFncml0dGUifQ=="></script>
```

This however does not support [Further customisation](#markdown-header-further-customisation).

## Further customisation
If you disable the default design, you're going to have to rewrite a new design otherwise nothing is going to get displayed. Neither the design's javacript nor the css will be downloaded and you will have to write it from scratch.

Fortunately the configuration script can provide you with everything you need to reimplement your own design: once the Social Hub is done being set up, it will return a class you can use to access the data you need.

### Documentation
The `pictawall.SocialHub` function returns an object containing an instance of classes `Display` and `Engine`.  
Detailed documentation for these classes is available under the `docs` folder.

### examples
```
#!javascript
// Loading mediums
pictawall.SocialHub({
    /* configuration */
    options: {
        // disable default design.
        _layout: false
    }
}).then(function ({ engine, display }) {
    // Request 25 new cards to display.
    engine.fetchMediums(25);

    // Receive asynchronously the requested cards.
    engine.addEventListener('newMedium', function (medium) {
        // Remove the loading bar
        display.setLoading(false);
        
        /* display medium however you want. */
    });
});
```

## Extreme customisation
Do not under any circumstances edit the contents of the `src` folder to customise the social hub. This will make it extremely difficult to maintain and update the hub as changes made to the default hub will have to be backported to your custom hub. Use instead what is detailed in the [Further customisation](#markdown-header-further-customisation) chapter.

You can, however, modify the source code to add hooks that can be used to customise. Try to keep your hooks as generic as possible and if at all possible, keep your changes backward-compatibles.

## Contributing
If you're the new maintainer of this project, or just come to contribute, welcome!  
This chapter will discuss the various guidelines in place and how to set up your workspace.

### Setting up
If you haven't done this already, please set up an SSH key for git on your computer. Then clone this repository in your workspace.

You will also need access to the `pictawall-sdk-javascript` respository.

This project uses `npm`, therefore you will need [NodeJS](https://nodejs.org/) installed on your computer.  
Once this is done, you just need to type `npm install` and npm will handle everything for you.

### Building
Building is handled by `gulp`, make sure it is installed on your computer or install it via npm: `npm install gulp -g`.  
There are then a few build script you can use:  

* `gulp` (default task): Builds the source code and launches BrowserSync. Your web page will automatically synchronise itself with any change you make in the source code.
* `gulp dist`: Builds the source code. If `NODE_ENV` is set to `production`, the code will also be minified. Built files are outputted to /dist
* `gulp jsdoc`: Builds the source code documentation.
* `gulp static-server`: If you don't want your web page to synchronise itself, use this instead of `gulp`. You will need to manually build your code and refresh your browser.
* `gulp config`: Synchronise bower.json with package.json (also part of the default task).
* `gulp test`: Runs karma

### Code Style
The source code is written following the [Airbnb JavaScript Style](https://github.com/airbnb/javascript) guidelines.

### Versioning
The project version follows the [Semantic Versioning](http://semver.org/) guidelines

> Given a version number MAJOR.MINOR.PATCH, increment the:  
> - MAJOR version when you make incompatible API changes,  
> - MINOR version when you add functionality in a backwards-compatible manner, and  
> - PATCH version when you make backwards-compatible bug fixes.  

Only update the version when releasing a new version of course.  
When releasing a new version, please tag the last commit for that version with the version number (`v{VERSION}` - ex: v1.0.0).  
Before releasing a new **Major** version, move the current source code (the source code as it is before the breaking changes) to a new branch called `Major_{MAJOR_VERSION}` (ex: Major_1).

You may delete branches of versions that aren't in use anymore.

### Creating a custom version for a client
The former way of creating a custom version for a client was to create a new branch on this repository. This will not be the case anymore. **A new branch should only be created on this repository prior to releasing a new ***MAJOR*** version** or for experimentation.

Branches containing client code should be moved to another repository. Doing it either as one branch per client (all of them in a single repository) or one repository per client is not defined by these guidelines, although I would recommend doing the latter.
